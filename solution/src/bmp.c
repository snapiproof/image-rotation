#include "../include/bmp.h"
#include <stdbool.h>
#include <stdint.h>

#include "../include/image.h"
#include "../include/util.h"

static uint8_t get_padding(uint64_t width) {
    return width % 4;
}

enum read_status from_bmp(FILE* fp, struct image* image) {
    struct header header;
    if(!fread(&header, sizeof(struct header), 1, fp)) return READ_INVALID_HEADER;

    if (header.biBitCount != 24) return READ_INVALID_BITS;
    if (header.bfType != 19778) return READ_INVALID_SIGNATURE;
    if (header.bOffBits != 54) return READ_INVALID_OFFSET;
    if (header.biSize != 40) return READ_INVALID_SIZE;

    image->width = header.biWidth;
    image->height = header.biHeight;
    image->data = malloc(sizeof(struct pixel) * image->height * image->width);

    uint8_t padding = get_padding(image->width);
    size_t row_count;
    for (size_t i=0; i < image->height; i++) {
        row_count = fread(&(image->data[i * image->width]), sizeof(struct pixel), image->width, fp);
        int check_fseek = fseek(fp, padding, SEEK_CUR);
        if (image->width != row_count || check_fseek != 0) return READ_INVALID_BITS;
    }

    return READ_OK;
}


enum write_status to_bmp(FILE* fp, struct image const* image) {
    uint32_t biSizeImage = image->width * image->height * sizeof(struct pixel) + image->height * get_padding(image->width);
    uint32_t biFileSize = sizeof(struct header) + biSizeImage;

    struct header header = {
            19778, biFileSize, 0, sizeof(struct header), 40,
            image->width, image->height, 1, 24, 0, biSizeImage,
            0, 0, 0, 0
    };

    if(!fwrite(&header, sizeof(struct header), 1, fp))
        return WRITE_ERROR_HEADER;

    uint8_t padding = get_padding(image->width);
    size_t row_count;
    const uint64_t paddingByte = 0;
    for (size_t i=0; i < image->height; i++) {
        row_count = fwrite(&(image->data[i * image->width]), sizeof(struct pixel), image->width, fp);
        fwrite(&paddingByte, 1, padding, fp); // write padding bytes

        if (image->width != row_count)
            return WRITE_ERROR_DATA;
    }
    return WRITE_OK;
}
