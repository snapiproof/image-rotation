#include "../include/util.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include "../include/rotate.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    FILE* file;
    enum file_status file_status = file_open_rb(argv[1], &file);
    if (file_status != FILE_OK) return file_status;

    struct image image;
    enum read_status read_status = from_bmp(file, &image);
    if (read_status != READ_OK) return read_status;

    file_status = file_close(file);
    if (file_status != FILE_OK) return file_status;

    struct image rotated_image = rotate(image);
    free(image.data);

    FILE* rotated_file;
    file_status = file_open_wb(argv[2], &rotated_file);
    if (file_status != FILE_OK) return file_status;

    enum write_status write_status = to_bmp(rotated_file, &rotated_image);
    if (write_status != WRITE_OK) return write_status;

    file_status = file_close(rotated_file);
    if (file_status != FILE_OK) return file_status;
    free(rotated_image.data);

    return 0;
}
