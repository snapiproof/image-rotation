#include "../include/rotate.h"

#include "../include/image.h"

struct image rotate(const struct image source) {
    struct image rotated = image_create(source.height, source.width);
    for (size_t i = 0; i < rotated.height; i++) {
        for (size_t j = 0; j < rotated.width; j++) {
            rotated.data[i*rotated.width+j] = source.data[(rotated.width - j - 1) * source.width + i];
        }
    }
    return rotated;
}
