#include "../include/util.h"

#include <stdio.h>
#include <stdlib.h>

enum file_status file_open_rb(const char* path, FILE** file) {
    fopen_s(file, path, "rb");
    if (*file == NULL)
        return FILE_ERROR;
    return FILE_OK;
}

enum file_status file_open_wb(const char* path, FILE** file) {
    fopen_s(file, path, "wb");
    if (*file == NULL)
        return FILE_ERROR;
    return FILE_OK;
}

enum file_status file_close(FILE* fp) {
    if (fclose(fp))
        return FILE_ERROR;
    return FILE_OK;
}
