#ifndef STRUCT_IMAGE
#define STRUCT_IMAGE

#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint64_t width, uint64_t height);
#endif