#ifndef UTIL
#define UTIL

#include <stdio.h>

enum file_status{
    FILE_OK = 0,
    FILE_ERROR
};

enum file_status file_open_rb(const char* path, FILE** file);
enum file_status file_open_wb(const char* path, FILE** file);
enum file_status file_close(FILE* fp);
#endif
