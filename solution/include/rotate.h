#ifndef ROTATE_IMAGE
#define ROTATE_IMAGE

#include "image.h"

struct image rotate(const struct image source);
#endif